package com.kaf.gestionstockmvc.services;

import java.util.List;

import com.kaf.gestionstockmvc.entites.Fournisseur;

public interface IFournisseurService {
	
public Fournisseur save(Fournisseur entity);
	
	public Fournisseur update(Fournisseur entity);
	
	public List<Fournisseur> selectAll();
	
	public List<Fournisseur> selectAll(String sortFile, String sort);
	
	public Fournisseur getById(Long id);
	
	public void remove(Long id);	
	
	public Fournisseur FindOne(String paramName, Object paramValue);
	
	public Fournisseur findOne(String[] paramNames, Object[] paramValues);
	
	public int findCountBy(String paramName, String paramValue);
	
	
	
}
