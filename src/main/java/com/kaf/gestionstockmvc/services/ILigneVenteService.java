package com.kaf.gestionstockmvc.services;

import java.util.List;

import com.kaf.gestionstockmvc.entites.LigneVente;

public interface ILigneVenteService {
	
public LigneVente save(LigneVente entity);
	
	public LigneVente update(LigneVente entity);
	
	public List<LigneVente> selectAll();
	
	public List<LigneVente> selectAll(String sortFile, String sort);
	
	public LigneVente getById(Long id);
	
	public void remove(Long id);	
	
	public LigneVente FindOne(String paramName, Object paramValue);
	
	public LigneVente findOne(String[] paramNames, Object[] paramValues);
	
	public int findCountBy(String paramName, String paramValue);
	
	
	
}
