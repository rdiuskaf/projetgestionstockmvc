package com.kaf.gestionstockmvc.services;

import java.io.InputStream;

public interface IFlickrService {

	public String savePhoto(InputStream stream, String title) throws Exception;
}
