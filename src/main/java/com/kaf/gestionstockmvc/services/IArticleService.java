package com.kaf.gestionstockmvc.services;

import java.util.List;

import com.kaf.gestionstockmvc.entites.Article;

public interface IArticleService {
	
public Article save(Article entity);
	
	public Article update(Article entity);
	
	public List<Article> selectAll();
	
	public List<Article> selectAll(String sortFile, String sort);
	
	public Article getById(Long id);
	
	public void remove(Long id);	
	
	public Article FindOne(String paramName, Object paramValue);
	
	public Article findOne(String[] paramNames, Object[] paramValues);
	
	public int findCountBy(String paramName, String paramValue);
	
	
	
}
