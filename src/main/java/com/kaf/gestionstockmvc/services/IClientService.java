package com.kaf.gestionstockmvc.services;

import java.util.List;

import com.kaf.gestionstockmvc.entites.Client;

public interface IClientService {
	
public Client save(Client entity);
	
	public Client update(Client entity);
	
	public List<Client> selectAll();
	
	public List<Client> selectAll(String sortFile, String sort);
	
	public Client getById(Long id);
	
	public void remove(Long id);	
	
	public Client FindOne(String paramName, Object paramValue);
	
	public Client findOne(String[] paramNames, Object[] paramValues);
	
	public int findCountBy(String paramName, String paramValue);
	
	
	
}
