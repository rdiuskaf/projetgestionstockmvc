package com.kaf.gestionstockmvc.services;

import java.util.List;

import com.kaf.gestionstockmvc.entites.CommandeClient;

public interface ICmdClientService {
	
	public CommandeClient save(CommandeClient entity);
	
	public CommandeClient update(CommandeClient entity);
	
	public List<CommandeClient> selectAll();
	
	public List<CommandeClient> selectAll(String sortFile, String sort);
	
	public CommandeClient getById(Long id);
	
	public void remove(Long id);	
	
	public CommandeClient FindOne(String paramName, Object paramValue);
	
	public CommandeClient findOne(String[] paramNames, Object[] paramValues);
	
	public int findCountBy(String paramName, String paramValue);
	
	
	
}
