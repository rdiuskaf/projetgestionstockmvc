package com.kaf.gestionstockmvc.services;

import java.util.List;

import com.kaf.gestionstockmvc.entites.LigneCommandeClient;

public interface ILigneCmdClientService {
	
	public LigneCommandeClient save(LigneCommandeClient entity);
	
	public LigneCommandeClient update(LigneCommandeClient entity);
	
	public List<LigneCommandeClient> selectAll();
	
	public List<LigneCommandeClient> selectAll(String sortFile, String sort);
	
	public LigneCommandeClient getById(Long id);
	
	public void remove(Long id);	
	
	public LigneCommandeClient FindOne(String paramName, Object paramValue);
	
	public LigneCommandeClient findOne(String[] paramNames, Object[] paramValues);
	
	public int findCountBy(String paramName, String paramValue);
	
	
	
}
