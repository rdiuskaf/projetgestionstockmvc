package com.kaf.gestionstockmvc.services;

import java.util.List;

import com.kaf.gestionstockmvc.entites.Mvstock;

public interface IMvstckService {
	
	public Mvstock save(Mvstock entity);
	
	public Mvstock update(Mvstock entity);
	
	public List<Mvstock> selectAll();
	
	public List<Mvstock> selectAll(String sortFile, String sort);
	
	public Mvstock getById(Long id);
	
	public void remove(Long id);	
	
	public Mvstock FindOne(String paramName, Object paramValue);
	
	public Mvstock findOne(String[] paramNames, Object[] paramValues);
	
	public int findCountBy(String paramName, String paramValue);
	
	
	
}
