package com.kaf.gestionstockmvc.services;

import java.util.List;

import com.kaf.gestionstockmvc.entites.Utilisateur;

public interface IUtilisateurService {
	
	public Utilisateur save(Utilisateur entity);
	
	public Utilisateur update(Utilisateur entity);
	
	public List<Utilisateur> selectAll();
	
	public List<Utilisateur> selectAll(String sortFile, String sort);
	
	public Utilisateur getById(Long id);
	
	public void remove(Long id);	
	
	public Utilisateur FindOne(String paramName, Object paramValue);
	
	public Utilisateur findOne(String[] paramNames, Object[] paramValues);
	
	public int findCountBy(String paramName, String paramValue);
	
	
	
}
