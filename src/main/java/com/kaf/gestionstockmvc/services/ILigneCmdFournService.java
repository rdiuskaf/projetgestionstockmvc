package com.kaf.gestionstockmvc.services;

import java.util.List;

import com.kaf.gestionstockmvc.entites.LigneCommandeFournisseur;

public interface ILigneCmdFournService {
	
public LigneCommandeFournisseur save(LigneCommandeFournisseur entity);
	
	public LigneCommandeFournisseur update(LigneCommandeFournisseur entity);
	
	public List<LigneCommandeFournisseur> selectAll();
	
	public List<LigneCommandeFournisseur> selectAll(String sortFile, String sort);
	
	public LigneCommandeFournisseur getById(Long id);
	
	public void remove(Long id);	
	
	public LigneCommandeFournisseur FindOne(String paramName, Object paramValue);
	
	public LigneCommandeFournisseur findOne(String[] paramNames, Object[] paramValues);
	
	public int findCountBy(String paramName, String paramValue);
	
	
	
}
