package com.kaf.gestionstockmvc.services;

import java.util.List;

import com.kaf.gestionstockmvc.entites.CommandeFournisseur;

public interface ICmdFournService {
	
	public CommandeFournisseur save(CommandeFournisseur entity);
	
	public CommandeFournisseur update(CommandeFournisseur entity);
	
	public List<CommandeFournisseur> selectAll();
	
	public List<CommandeFournisseur> selectAll(String sortFile, String sort);
	
	public CommandeFournisseur getById(Long id);
	
	public void remove(Long id);	
	
	public CommandeFournisseur FindOne(String paramName, Object paramValue);
	
	public CommandeFournisseur findOne(String[] paramNames, Object[] paramValues);
	
	public int findCountBy(String paramName, String paramValue);
	
	
	
}
