package com.kaf.gestionstockmvc.services.Impl;

import java.util.List;

import com.kaf.gestionstockmvc.Dao.ICategoryDAO;
import com.kaf.gestionstockmvc.entites.Categorie;
import com.kaf.gestionstockmvc.services.ICategorieService;

public class CategorieServiceImpl implements ICategorieService {

	private ICategoryDAO dao;	
	
	@Override
	public Categorie save(Categorie entity) {
		// TODO Auto-generated method stub
		return dao.save(entity);
	}

	@Override
	public Categorie update(Categorie entity) {
		// TODO Auto-generated method stub
		return dao.update(entity);
	}

	@Override
	public List<Categorie> selectAll() {
		// TODO Auto-generated method stub
		return dao.selectAll();
	}

	@Override
	public List<Categorie> selectAll(String sortFile, String sort) {
		// TODO Auto-generated method stub
		return dao.selectAll(sortFile, sort);
	}

	@Override
	public Categorie getById(Long id) {
		// TODO Auto-generated method stub
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		dao.remove(id);
		
	}

	@Override
	public Categorie FindOne(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.FindOne(paramName, paramValue);
	}

	@Override
	public Categorie findOne(String[] paramNames, Object[] paramValues) {
		// TODO Auto-generated method stub
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		// TODO Auto-generated method stub
		return dao.findCountBy(paramName, paramValue);
	}

	public ICategoryDAO getDao() {
		return dao;
	}

	public void setDao(ICategoryDAO dao) {
		this.dao = dao;
	}

}
