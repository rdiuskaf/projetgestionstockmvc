package com.kaf.gestionstockmvc.services.Impl;

import java.util.List;

import com.kaf.gestionstockmvc.Dao.IVenteDAO;
import com.kaf.gestionstockmvc.entites.Vente;
import com.kaf.gestionstockmvc.services.IVenteService;


//@Transactional
public class VenteServiceImpl implements IVenteService {
	
	private IVenteDAO dao;

	public IVenteDAO getDao() {
		return dao;
	}

	public void setDao(IVenteDAO dao) {
		this.dao = dao;
	}

	@Override
	public Vente save(Vente entity) {
		
		return dao.save(entity);
	}

	@Override
	public Vente update(Vente entity) {
		return dao.update(entity);
	}

	@Override
	public List<Vente> selectAll() {
		
		return dao.selectAll();
	}

	@Override
	public List<Vente> selectAll(String sortFile, String sort) {
		
		return dao.selectAll(sortFile, sort);
	}

	@Override
	public Vente getById(Long id) {
		
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	@Override
	public Vente FindOne(String paramName, Object paramValue) {
		
		return dao.FindOne(paramName, paramValue);
	}

	@Override
	public Vente findOne(String[] paramNames, Object[] paramValues) {
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}
	
	
}
