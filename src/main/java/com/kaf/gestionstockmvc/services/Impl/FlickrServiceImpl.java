package com.kaf.gestionstockmvc.services.Impl;

import java.io.InputStream;

import com.kaf.gestionstockmvc.Dao.IFlickrDAO;
import com.kaf.gestionstockmvc.services.IFlickrService;

public class FlickrServiceImpl implements IFlickrService{

	private IFlickrDAO dao;
	
	@Override
	public String savePhoto(InputStream photo, String title) throws Exception {
		// TODO Auto-generated method stub
		return dao.SavePhoto(photo, title);
	}

	public IFlickrDAO getDao() {
		return dao;
	}

	public void setDao(IFlickrDAO dao) {
		this.dao = dao;
	}

}
