package com.kaf.gestionstockmvc.services.Impl;

import java.util.List;

import com.kaf.gestionstockmvc.Dao.ILigneCommandeClientDAO;
import com.kaf.gestionstockmvc.entites.LigneCommandeClient;
import com.kaf.gestionstockmvc.services.ILigneCmdClientService;

public class ligneCmdClientServiceImpl implements ILigneCmdClientService{

	private ILigneCommandeClientDAO dao;
	
	public ILigneCommandeClientDAO getDao() {
		return dao;
	}

	public void setDao(ILigneCommandeClientDAO dao) {
		this.dao = dao;
	}

	@Override
	public LigneCommandeClient save(LigneCommandeClient entity) {
		// TODO Auto-generated method stub
		return dao.save(entity);
	}

	@Override
	public LigneCommandeClient update(LigneCommandeClient entity) {
		// TODO Auto-generated method stub
		return dao.update(entity);
	}

	@Override
	public List<LigneCommandeClient> selectAll() {
		// TODO Auto-generated method stub
		return dao.selectAll();
	}

	@Override
	public List<LigneCommandeClient> selectAll(String sortFile, String sort) {
		// TODO Auto-generated method stub
		return dao.selectAll(sortFile, sort);
	}

	@Override
	public LigneCommandeClient getById(Long id) {
		// TODO Auto-generated method stub
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		dao.remove(id);
		
	}

	@Override
	public LigneCommandeClient FindOne(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.FindOne(paramName, paramValue);
	}

	@Override
	public LigneCommandeClient findOne(String[] paramNames, Object[] paramValues) {
		// TODO Auto-generated method stub
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		// TODO Auto-generated method stub
		return dao.findCountBy(paramName, paramValue);
	}

	
}
