package com.kaf.gestionstockmvc.services.Impl;

import java.util.List;

import com.kaf.gestionstockmvc.Dao.IArticleDAO;
import com.kaf.gestionstockmvc.entites.Article;
import com.kaf.gestionstockmvc.services.IArticleService;


//@Transactional
public class ArticleServiceImpl implements IArticleService {
	
	private IArticleDAO dao;

	@Override
	public Article save(Article entity) {
		return dao.save(entity);
	}

	@Override
	public Article update(Article entity) {
		return dao.update(entity);
	}

	@Override
	public List<Article> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<Article> selectAll(String sortFile, String sort) {
		return dao.selectAll(sortFile, sort);
	}

	@Override
	public Article getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
		
	}

	@Override
	public Article FindOne(String paramName, Object paramValue) {
		return dao.FindOne(paramName, paramValue);
	}

	@Override
	public Article findOne(String[] paramNames, Object[] paramValues) {
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}

	public IArticleDAO getDao() {
		return dao;
	}

	public void setDao(IArticleDAO dao) {
		this.dao = dao;
	}

}
