package com.kaf.gestionstockmvc.services.Impl;

import java.util.List;

import com.kaf.gestionstockmvc.Dao.ICommandeClientDAO;
import com.kaf.gestionstockmvc.entites.CommandeClient;
import com.kaf.gestionstockmvc.services.ICmdClientService;

public class CmdClientServiceImpl implements ICmdClientService {

	private ICommandeClientDAO dao;
	
	@Override
	public CommandeClient save(CommandeClient entity) {
		// TODO Auto-generated method stub
		return dao.save(entity);
	}

	@Override
	public CommandeClient update(CommandeClient entity) {
		// TODO Auto-generated method stub
		return dao.update(entity);
	}

	@Override
	public List<CommandeClient> selectAll() {
		// TODO Auto-generated method stub
		return dao.selectAll();
	}

	@Override
	public List<CommandeClient> selectAll(String sortFile, String sort) {
		// TODO Auto-generated method stub
		return dao.selectAll(sortFile, sort);
	}

	@Override
	public CommandeClient getById(Long id) {
		// TODO Auto-generated method stub
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		dao.remove(id);
		
	}

	@Override
	public CommandeClient FindOne(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.FindOne(paramName, paramValue);
	}

	@Override
	public CommandeClient findOne(String[] paramNames, Object[] paramValues) {
		// TODO Auto-generated method stub
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		// TODO Auto-generated method stub
		return dao.findCountBy(paramName, paramValue);
	}

	public ICommandeClientDAO getDao() {
		return dao;
	}

	public void setDao(ICommandeClientDAO dao) {
		this.dao = dao;
	}

}
