package com.kaf.gestionstockmvc.services.Impl;

import java.util.List;

import com.kaf.gestionstockmvc.Dao.IMvStckDAO;
import com.kaf.gestionstockmvc.entites.Mvstock;

public class MvstockServiceImpl implements IMvStckDAO {
	
	private IMvStckDAO dao;

	@Override
	public Mvstock save(Mvstock entity) {
		// TODO Auto-generated method stub
		return dao.save(entity);
	}

	@Override
	public Mvstock update(Mvstock entity) {
		// TODO Auto-generated method stub
		return dao.update(entity);
	}

	@Override
	public List<Mvstock> selectAll() {
		// TODO Auto-generated method stub
		return dao.selectAll();
	}

	@Override
	public List<Mvstock> selectAll(String sortFile, String sort) {
		// TODO Auto-generated method stub
		return dao.selectAll(sortFile, sort);
	}

	@Override
	public Mvstock getById(Long id) {
		// TODO Auto-generated method stub
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		dao.remove(id);
		
	}

	@Override
	public Mvstock FindOne(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.FindOne(paramName, paramValue);
	}

	@Override
	public Mvstock findOne(String[] paramNames, Object[] paramValues) {
		// TODO Auto-generated method stub
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		// TODO Auto-generated method stub
		return dao.findCountBy(paramName, paramValue);
	}

	public IMvStckDAO getDao() {
		return dao;
	}

	public void setDao(IMvStckDAO dao) {
		this.dao = dao;
	}

}
