package com.kaf.gestionstockmvc.services.Impl;

import java.util.List;

import com.kaf.gestionstockmvc.Dao.IUtilisateurDAO;
import com.kaf.gestionstockmvc.entites.Utilisateur;
import com.kaf.gestionstockmvc.services.IUtilisateurService;

public class UtilisateurServiceImpl implements IUtilisateurService {

	private IUtilisateurDAO dao;
	
	@Override
	public Utilisateur save(Utilisateur entity) {
		
		return dao.save(entity);
	}

	@Override
	public Utilisateur update(Utilisateur entity) {
	
		return dao.update(entity);
	}

	@Override
	public List<Utilisateur> selectAll() {
		
		return dao.selectAll();
	}

	@Override
	public List<Utilisateur> selectAll(String sortFile, String sort) {
		
		return dao.selectAll(sortFile, sort) ;
	}

	@Override
	public Utilisateur getById(Long id) {
	
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
		
	}

	@Override
	public Utilisateur FindOne(String paramName, Object paramValue) {
		return dao.FindOne(paramName, paramValue);
	}

	@Override
	public Utilisateur findOne(String[] paramNames, Object[] paramValues) {
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		// TODO Auto-generated method stub
		return dao.findCountBy(paramName, paramValue);
	}

	public IUtilisateurDAO getDao() {
		return dao;
	}

	public void setDao(IUtilisateurDAO dao) {
		this.dao = dao;
	}

}
