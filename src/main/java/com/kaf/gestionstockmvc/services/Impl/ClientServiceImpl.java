package com.kaf.gestionstockmvc.services.Impl;

import java.util.List;

import com.kaf.gestionstockmvc.Dao.IClientDAO;
import com.kaf.gestionstockmvc.entites.Client;
import com.kaf.gestionstockmvc.services.IClientService;

public class ClientServiceImpl implements IClientService {
	
	private IClientDAO dao;

	@Override
	public Client save(Client entity) {
		
		return dao.save(entity);
	}

	@Override
	public Client update(Client entity) {
		
		return dao.update(entity);
	}

	@Override
	public List<Client> selectAll() {
		
		return dao.selectAll();
	}

	@Override
	public List<Client> selectAll(String sortFile, String sort) {
		
		return dao.selectAll(sortFile, sort);
	}

	@Override
	public Client getById(Long id) {
		
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		
		dao.remove(id);
		
	}

	@Override
	public Client FindOne(String paramName, Object paramValue) {
		
		return dao.FindOne(paramName, paramValue);
	}

	@Override
	public Client findOne(String[] paramNames, Object[] paramValues) {
		
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		
		return dao.findCountBy(paramName, paramValue);
	}

	public IClientDAO getDao() {
		return dao;
	}

	public void setDao(IClientDAO dao) {
		this.dao = dao;
	}

	
	
}
