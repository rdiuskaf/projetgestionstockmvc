package com.kaf.gestionstockmvc.services.Impl;

import java.util.List;

import com.kaf.gestionstockmvc.Dao.ILigneVenteDAO;
import com.kaf.gestionstockmvc.entites.LigneVente;
import com.kaf.gestionstockmvc.services.ILigneVenteService;

public class LigneVenteServiceImpl implements ILigneVenteService {

	private ILigneVenteDAO dao;
	
	@Override
	public LigneVente save(LigneVente entity) {
		// TODO Auto-generated method stub
		return dao.save(entity);
	}

	@Override
	public LigneVente update(LigneVente entity) {
		// TODO Auto-generated method stub
		return dao.update(entity);
	}

	@Override
	public List<LigneVente> selectAll() {
		// TODO Auto-generated method stub
		return dao.selectAll();
	}

	@Override
	public List<LigneVente> selectAll(String sortFile, String sort) {
		// TODO Auto-generated method stub
		return dao.selectAll(sortFile, sort);
	}

	@Override
	public LigneVente getById(Long id) {
		// TODO Auto-generated method stub
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		dao.remove(id);
		
	}

	@Override
	public LigneVente FindOne(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.FindOne(paramName, paramValue);
	}

	@Override
	public LigneVente findOne(String[] paramNames, Object[] paramValues) {
		// TODO Auto-generated method stub
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		// TODO Auto-generated method stub
		return dao.findCountBy(paramName, paramValue);
	}

	public ILigneVenteDAO getDao() {
		return dao;
	}

	public void setDao(ILigneVenteDAO dao) {
		this.dao = dao;
	}

}
