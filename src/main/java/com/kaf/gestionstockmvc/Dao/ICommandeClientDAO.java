package com.kaf.gestionstockmvc.Dao;

import com.kaf.gestionstockmvc.entites.CommandeClient;

public interface ICommandeClientDAO extends IGenericDao<CommandeClient> {

}
