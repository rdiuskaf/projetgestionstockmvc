package com.kaf.gestionstockmvc.Dao;

import com.kaf.gestionstockmvc.entites.LigneCommandeClient;

public interface ILigneCommandeClientDAO extends IGenericDao<LigneCommandeClient> {

}
