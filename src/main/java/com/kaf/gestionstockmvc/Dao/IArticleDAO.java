package com.kaf.gestionstockmvc.Dao;

import com.kaf.gestionstockmvc.entites.Article;

public interface IArticleDAO extends IGenericDao<Article>{

}
