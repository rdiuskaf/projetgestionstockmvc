package com.kaf.gestionstockmvc.Dao;

import java.io.InputStream;

public interface IFlickrDAO {
	
	public String SavePhoto(InputStream stream, String fileName) throws Exception;
	
}
