package com.kaf.gestionstockmvc.Dao.Imp;

import java.io.InputStream;
import java.util.concurrent.ExecutionException;

import javax.swing.JOptionPane;

import org.scribe.model.Verifier;

import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.FlickrException;
import com.flickr4java.flickr.REST;
import com.flickr4java.flickr.RequestContext;
import com.flickr4java.flickr.auth.Auth;
import com.flickr4java.flickr.auth.AuthInterface;
import com.flickr4java.flickr.auth.Permission;
import com.flickr4java.flickr.uploader.UploadMetaData;
import com.kaf.gestionstockmvc.Dao.IFlickrDAO;

import antlr.Token;

public class FlickrDAOImpl implements IFlickrDAO {
	
	private Flickr flickr;
	
	private UploadMetaData uploadMetaData = new UploadMetaData();
	private String apikey ="bc154cb5297430493a631c65c68f7dff";
	private String sharedSecret= "7bdafea616c9a8c5";
	
	
	private void connect() {
		flickr = new Flickr (apikey, sharedSecret, new REST());
		Auth auth =new Auth();
		auth.setPermission(Permission.READ);
		auth.setToken("72157694841761885-abf2cbb4cc21bfc4");
		auth.setTokenSecret("b12a6ccdf9c47f59");
		RequestContext requestContext= RequestContext.getRequestContext();
		requestContext.setAuth(auth);
		flickr.setAuth(auth);
	}
	
	@Override
	public String SavePhoto(InputStream photo, String title) throws Exception {
		connect();
		uploadMetaData.setTitle(title);
		String photoId= flickr.getUploader().upload(photo, uploadMetaData);
		return flickr.getPhotosInterface().getPhoto(photoId).getMedium640Url();
	}
	
	public void auth() {
		flickr= new Flickr(apikey,sharedSecret, new REST());
		AuthInterface authInterface = flickr.getAuthInterface();
		
		org.scribe.model.Token token = authInterface.getRequestToken();
		System.out.println("token: " +token);
		
		String url = authInterface.getAuthorizationUrl(token, Permission.DELETE);
		System.out.println("Folow this url to autorize yourself on flickr");
		System.out.println(url);
		System.out.println("Paste in the tocken it gives you :");
		System.out.println(">>");
		
		String tokenkey = JOptionPane.showInputDialog(null);
		
		org.scribe.model.Token requestToken = authInterface.getAccessToken(token, new Verifier(tokenkey));
		System.out.println("Authentication success");
		
		Auth auth = null;
		
		try {
			auth = authInterface.checkToken(requestToken);			
		} catch (FlickrException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		System.out.println("Token: " + requestToken.getToken());
		System.out.println("Secret: " +requestToken.getSecret());
		System.out.println("Userid: " +auth.getUser().getId());
		System.out.println("Realname: " +auth.getUser().getRealName());
		System.out.println("UserName: " +auth.getUser().getUsername());
		System.out.println("Permission: " + auth.getPermission().getType());
	}	
	

}


//token: Token[72157694089908634-c2644dde889fa607 , 48d804b0f87b0537]
//Folow this url to autorize yourself on flickr
//http://www.flickr.com/services/oauth/authorize?oauth_token=72157694089908634-c2644dde889fa607&perms=delete
//Paste in the tocken it gives you :
//>>
//Authentication success
//Token: 72157694841761885-abf2cbb4cc21bfc4
//Secret: b12a6ccdf9c47f59
//Userid: 160439244@N05
//Realname: kafando rodrique
//UserName: rdiuskaf
//Permission: 3



