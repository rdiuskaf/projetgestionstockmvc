package com.kaf.gestionstockmvc.Dao;

import com.kaf.gestionstockmvc.entites.Fournisseur;

public interface IFournisseurDAO extends IGenericDao<Fournisseur> {

}
