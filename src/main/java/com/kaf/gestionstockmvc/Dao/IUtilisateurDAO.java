package com.kaf.gestionstockmvc.Dao;

import com.kaf.gestionstockmvc.entites.Utilisateur;

public interface IUtilisateurDAO extends IGenericDao<Utilisateur> {

}
