package com.kaf.gestionstockmvc.Dao;

import com.kaf.gestionstockmvc.entites.Categorie;

public interface ICategoryDAO extends IGenericDao<Categorie>{

}
