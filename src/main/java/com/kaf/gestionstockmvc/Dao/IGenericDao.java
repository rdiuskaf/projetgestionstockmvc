package com.kaf.gestionstockmvc.Dao;

import java.util.List;

public interface IGenericDao<E> {
	
	public E save(E entity);
	
	public E update(E entity);
	
	public List<E> selectAll();
	
	public List<E> selectAll(String sortFile, String sort);
	
	public E getById(Long id);
	
	public void remove(Long id);	
	
	public E FindOne(String paramName, Object paramValue);
	
	public E findOne(String[] paramNames, Object[] paramValues);
	
	public int findCountBy(String paramName, String paramValue);
	
	

}
