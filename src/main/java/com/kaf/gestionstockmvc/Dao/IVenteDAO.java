package com.kaf.gestionstockmvc.Dao;

import com.kaf.gestionstockmvc.entites.Vente;

public interface IVenteDAO extends IGenericDao<Vente> {

}
