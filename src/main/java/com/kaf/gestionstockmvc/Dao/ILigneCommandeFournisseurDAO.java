package com.kaf.gestionstockmvc.Dao;

import com.kaf.gestionstockmvc.entites.LigneCommandeFournisseur;

public interface ILigneCommandeFournisseurDAO extends IGenericDao<LigneCommandeFournisseur> {

}
