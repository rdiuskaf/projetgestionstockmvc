package com.kaf.gestionstockmvc.Dao;

import com.kaf.gestionstockmvc.entites.LigneVente;

public interface ILigneVenteDAO extends IGenericDao<LigneVente> {

}
