package com.kaf.gestionstockmvc.Dao;

import com.kaf.gestionstockmvc.entites.Client;

public interface IClientDAO extends IGenericDao<Client>{

}
