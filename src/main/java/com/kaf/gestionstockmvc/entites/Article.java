package com.kaf.gestionstockmvc.entites;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table (name="article")
public class Article implements Serializable {
	
	@Id
	@GeneratedValue
	private Long idArticle;
	private String codeArticle;
	private String Designation;
	private BigDecimal prixUnitaireHT;
	private BigDecimal tauxTva;
	private BigDecimal prixUnitaireTTC;
	private String photo;
	
	@ManyToOne
	@JoinColumn (name="idCategorie")
	private Categorie categorie;
	
	
	public Article() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Article(Long idArticle, String codeArticle, String designation, BigDecimal prixUnitaireHT,
			BigDecimal tauxTva, BigDecimal prixUnitaireTTC, String photo, Categorie categorie) {
		super();
		this.idArticle = idArticle;
		this.codeArticle = codeArticle;
		Designation = designation;
		this.prixUnitaireHT = prixUnitaireHT;
		this.tauxTva = tauxTva;
		this.prixUnitaireTTC = prixUnitaireTTC;
		this.photo = photo;
		this.categorie = categorie;
	}
	
	
	public Long getIdArticle() {
		return idArticle;
	}
	public void setIdArticle(Long idArticle) {
		this.idArticle = idArticle;
	}
	public String getCodeArticle() {
		return codeArticle;
	}
	public void setCodeArticle(String codeArticle) {
		this.codeArticle = codeArticle;
	}
	public String getDesignation() {
		return Designation;
	}
	public void setDesignation(String designation) {
		Designation = designation;
	}
	public BigDecimal getPrixUnitaireHT() {
		return prixUnitaireHT;
	}
	public void setPrixUnitaireHT(BigDecimal prixUnitaireHT) {
		this.prixUnitaireHT = prixUnitaireHT;
	}
	public BigDecimal getTauxTva() {
		return tauxTva;
	}
	public void setTauxTva(BigDecimal tauxTva) {
		this.tauxTva = tauxTva;
	}
	public BigDecimal getPrixUnitaireTTC() {
		return prixUnitaireTTC;
	}
	public void setPrixUnitaireTTC(BigDecimal prixUnitaireTTC) {
		this.prixUnitaireTTC = prixUnitaireTTC;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public Categorie getCategorie() {
		return categorie;
	}
	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}
	@Override
	public String toString() {
		return "Article [idArticle=" + idArticle + ", codeArticle=" + codeArticle + ", Designation=" + Designation
				+ ", prixUnitaireHT=" + prixUnitaireHT + ", tauxTva=" + tauxTva + ", prixUnitaireTTC=" + prixUnitaireTTC
				+ ", photo=" + photo + ", categorie=" + categorie + "]";
	}
	
	

}
