package com.kaf.gestionstockmvc.entites;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table (name="Mvstock")
public class Mvstock implements Serializable {
	
	public static final int ENTREE=1;
	public static final int SORTIE=2;	
	@Id
	@GeneratedValue
	private Long idMvstock;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;
	
	private BigDecimal quantite;
	private int typeMvt;
	
	@ManyToOne
	@JoinColumn(name="idArticle")
	private Article article;
	

	public Mvstock() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public BigDecimal getQuantite() {
		return quantite;
	}

	public void setQuantite(BigDecimal quantite) {
		this.quantite = quantite;
	}

	public int getTypeMvt() {
		return typeMvt;
	}

	public void setTypeMvt(int typeMvt) {
		this.typeMvt = typeMvt;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public static int getEntree() {
		return ENTREE;
	}

	public static int getSortie() {
		return SORTIE;
	}

	public Long getIdMvstock() {
		return idMvstock;
	}

	public void setIdMvstock(Long idMvstock) {
		this.idMvstock = idMvstock;
	}

	

}
