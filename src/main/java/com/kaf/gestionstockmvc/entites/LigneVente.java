package com.kaf.gestionstockmvc.entites;

import java.io.Serializable;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table (name="LigneVente")
public class LigneVente implements Serializable {
	@Id
	@GeneratedValue
	private Long idLigneVente;
	
	@ManyToOne
	@JoinColumn (name="idArticle")
	private Article article;
	
	
	@ManyToOne
	@JoinColumn (name="vent")
	private Vente vente;
	
	

	public LigneVente() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public Long getIdLigneVente() {
		return idLigneVente;
	}

	public void setIdLigneVente(Long idLigneVente) {
		this.idLigneVente = idLigneVente;
	}

	

}
